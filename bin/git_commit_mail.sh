#!/bin/sh

#
# merge exim.org/github repos
#
export GIT_DIR=/srv/git/exim.git
export GIT_KEY=/opt/cumin/ssh/git_repo_sync
export GIT_SSH=/opt/cumin/services/mirror-monitor/bin/gitssh.sh
git fetch --quiet github
git push --all --force github 2>&1 | fgrep -v 'Everything up-to-date'
git push --tags github 2>&1 | fgrep -v 'Everything up-to-date'

#
# base config for mailing
#
export FROM="Exim Git Commits Mailing List <exim-cvs@lists.exim.org>"
export SENDMAIL=/usr/sbin/exim

#
# Mail out updates for exim git
#
export MAILTAG=/opt/cumin/state/git-mail-exim
export MLIST=exim-cvs@lists.exim.org
export GITWEB=https://git.exim.org/exim.git
flock  $0 /opt/cumin/services/misc-git-hacks/git-feed-mail-list.sh

#
# Mail out updates for exim website git
#
export GIT_DIR=/srv/git/exim-website.git
export MAILTAG=/opt/cumin/state/git-mail-exim-website
export MLIST=exim-cvs@lists.exim.org
export GITWEB=https://git.exim.org/exim-website.git
flock  $0 /opt/cumin/services/misc-git-hacks/git-feed-mail-list.sh

# end
