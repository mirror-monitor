#!/bin/sh

export LC_MESSAGES="en_US.UTF-8"

cd /opt/cumin/services/nm4/exim-website || exit

git pull | grep -q -v 'Already up to date\.' && script/build.sh

cd /opt/cumin/services/mirror-monitor || exit

git pull --quiet
